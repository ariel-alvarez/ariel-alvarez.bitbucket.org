directory.PublicacionesView = Backbone.View.extend({

    render: function () {
        this.$el.html(this.template());
        var self = this;

        PostsHome.findByCategory(Categorias.PUBLICACIONES_LIBROS, function (col) {
            console.log(col);
            self.renderizarPublicaciones(".publicaciones-categoria-libros div.publicaciones-posts", col);
        });
        PostsHome.findByCategory(Categorias.PUBLICACIONES_COLABORACIONES, function (col) {
            self.renderizarPublicaciones(".publicaciones-categoria-colaboraciones div.publicaciones-posts", col);
        });
        PostsHome.findByCategory(Categorias.PUBLICACIONES_TRABAJOS_PUBLICADOS, function (col) {
            self.renderizarPublicaciones(".publicaciones-categoria-trabajos-publicados div.publicaciones-posts", col);
        });
        PostsHome.findByCategory(Categorias.PUBLICACIONES_TRABAJOS, function (col) {
            self.renderizarPublicaciones(".publicaciones-categoria-trabajos div.publicaciones-posts", col);
        });
        RenderTwitter();

        $("#publicaciones-navigator").find("> li").click(function(){
            $("#publicaciones-navigator").find("> li").removeClass("seleccionada");
            var self = $(this);
            self.addClass("seleccionada");
            showSeccion(self.data("seccion"));
        });

        var filtroPorSeccion = findUrlParameter("filtro");
        if(filtroPorSeccion){
            $('[data-seccion="'+filtroPorSeccion+'"]').click();
        }

        return this;
    },
    renderizarPublicaciones: function(selector, col){
        var self = this;
        var container = jQuery(selector);
        console.log(col);
        col.each(function(pub){
            container.append(self.renderizarPublicacion(pub));
        });
    },
    renderizarPublicacion: function(pub){
        var downloadAnchor = this.genAnchorFrom(pub, "link_descarga");
        var compraAnchor = this.genAnchorFrom(pub, "link_compra");
        var iconos = $('<div class="iconos"></div>');
        iconos.append($("<a></a>").attr('href',pub.getFacebookShareLink()).append($('<img src="img/publ_iconos01.png">')));
        iconos.append($("<a></a>").attr('href',pub.getTwitterShareLink()).append($('<img src="img/publ_iconos02.png">')));

        if(downloadAnchor){
            iconos.append(downloadAnchor.append($('<img src="img/publ_iconos04.png">')));
        }

        if(compraAnchor){
            iconos.append(compraAnchor.append($('<img src="img/publ_iconos03.png">')));
        }

        var content =  jQuery("<div class='publicaciones-post-container'></div>").append(
            jQuery("<div class='publicacion-post-title'></div>").append(pub.get("title"))
        ).append(
            jQuery("<div class='publicacion-post-desc'></div>").append(pub.get("excerpt"))
            ).append(iconos);

        var thumb = pub.getImageUrl();
        if(thumb) content.prepend(jQuery("<img src='"+thumb+"' class='publicacion-image'/>"));
        return jQuery("<div class='publicacion-item'></div>").append(content);
    },
    genAnchorFrom: function(pub, field){
        var cf = pub.get("custom_fields");
        console.log(cf[field]);
        if(cf[field] && cf[field][0]){
            return $('<a></a>').attr('href', cf[field][0]).attr('target', "_blank");
        }else{
            return null;
        }
    }

});

function showSeccion(seccion){
    if(seccion){
        $("#publicaciones-post-list").find("> div").hide();
        $("."+seccion).show();
    }else{
        $("#publicaciones-post-list").find("> div").show();
    }
}