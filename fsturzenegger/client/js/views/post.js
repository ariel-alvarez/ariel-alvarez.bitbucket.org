directory.PostView = Backbone.View.extend({

    initialize:function(){

    },
    render: function (id) {

        this.$el.html(this.template());
        PostsHome.all(function(self){
            var post = self.get(id);
            if(post.getImageUrl()){
                $(".post-image")
                    .append($("<img/>").attr("src", post.getImageUrl()))
                    .append(post.getImageCaption()? $('<div class="epigrafe text-left"></div>').text(post.getImageCaption()): "");
            }

            jQuery(".blog-post-fecha").append(post.getFechaPublicacion());

            var fuente = post.getCustomField("fuente");
            if(!fuente.none){
                jQuery(".blog-post-fuente").append("Fuente: " + fuente);
            }

            jQuery(".post-title").append(post.get("title"));
            jQuery(".post-excerpt").append(post.get("excerpt"));
            jQuery(".post-content").append(post.get("content"));

            $("#post-share-mail").attr("href", "mailto:?subject=" + encodeURIComponent(post.get("title")+ "&amp;body=" +encodeURIComponent(document.URL)));
            $("#post-share-facebook").attr('href', post.getFacebookShareLink());
            $("#post-share-twitter").attr('href', post.getTwitterShareLink());

            var tagsContainer = jQuery(".post-tags").html("<ul></ul>").find(">:first-child");
            _(post.get("tags")).each(function(tag){
               tagsContainer.append(jQuery("<li class='post-tag'></li>").text(tag.title));
            });

            PostsHome.findByCategory(post.getMainCategorySlug(), function(self){
                    var notDisplayedPosts = _(self.filter(function(p){ return p != post; }));

                    _(notDisplayedPosts.take(3)).each(function(aPost){
                        renderRelacionado(aPost);
                    });

                    var tagIDs = _(_(post.get("tags")).map(function(e){ return e.id;}));

                    _(_(notDisplayedPosts.sortBy(function(p){
                        return _.reduce(p.get("tags"), function(memo, e){
                            if(tagIDs.contains(e.id)){
                                return memo + 1;
                            }else{
                                return memo;
                            }
                        }, 0);

                    }).reverse()).take(3)).each(function(aPost, i){
                        buildPostBox(".post-novedad-box-"+(i+1), aPost, i==1?'#FFCC00': false );
                    });

                }
            );
        });



        $("html, body").animate({ scrollTop: 0 }, "slow");
        $("#post-share-facebook").attr("href", "https://www.facebook.com/sharer/sharer.php?u=" +encodeURIComponent(document.URL));
        $("#post-share-twitter").attr("href", "https://twitter.com/home?status=" +encodeURIComponent(document.URL));
        $("#post-button-print").click(function(){window.print();});

        HideTwitterFeedAndShowNavigation();
        return this;
    }
});

function renderRelacionado(post){
    var relacionado = $(".post-left-panel").append($('<div class="post-relacionado-lateral col-md-12 text-left"></div>'));
    var relacionadoExcerpt = $('<div class="lateral-excerpt"></div>').append(post.getTitleAsAnchor());
    relacionadoExcerpt.addClass("col-md-12");
    relacionado.append(relacionadoExcerpt);
}