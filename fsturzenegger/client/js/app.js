//TODO: utilizar underscore templates para los footers

var COLORES = {
  BLANCO: "#FFFFFF",
    GRIS_CLARO: "#F5F5F5",
    GRIS_OSCURO: "#E5E5E5",
    AMARILLO: "#FFCC00"
};

var directory = {

    views: {},

    models: {},

    loadTemplates: function (views, callback) {

        var deferreds = [];

        $.each(views, function (index, view) {
            if (directory[view]) {
                deferreds.push($.get('tpl/' + view + '.html', function (data) {
                    directory[view].prototype.template = _.template(data);
                }, 'html'));
            } else {
                console.log(view + " no encontrada");
            }
        });
        $.when.apply(null, deferreds).done(callback);
    }

};

var PaginasHome = {
    paginas: [
        "Home",
        "SobreMi",
        "Blog",
        "Legislatura",
        "Novedades",
        "Prensa",
        "Post",
        "Publicaciones",
        "Shell",
        "Busqueda",
        "Propuesta"
    ],
    getViews:function(){
        return this.paginas.map(function(e){ return e + "View"});
    }

};

var rutas = {

    routes: {
        "": "home",
        "sobremi": "sobremi",
        "blog": "blog",
        "legislatura": "legislatura",
        "prensa": "prensa",
        "novedades": "novedades",
        "publicaciones": "publicaciones",
        "busqueda": "busqueda",
        "propuesta": "propuesta",
        "post/:id/:slug" : "post"
    },

    initialize: function () {
        directory.shellView = new directory.ShellView();
        var body = $('body');
        body.html(directory.shellView.render().el);
        // Close the search dropdown on click anywhere in the UI
        body.click(function () {
            $('.dropdown').removeClass("open");
        });
        this.$content = $("#content");
    }
};

PaginasHome.paginas.forEach(function(e){
    rutas[e.toLowerCase()] = function(){
        var className = e + "View";
        var viewName = className.charAt(0).toLowerCase() + className.slice(1);

        if (!directory[viewName]) {
            console.log(className, viewName);
            directory[viewName] = new directory[className]();
        }
        this.$content.html(directory[viewName].el);
        directory[viewName].render();
        directory.shellView.selectMenuItem(e.toLowerCase()+'-menu');
        if(document.body.scrollTop) $("html, body").animate({ scrollTop: 0 }, "slow");
    }
});

rutas['post'] = function(id, slug){

    if(!directory.postsView){
        console.log(directory);
        directory.postsView = new directory["PostView"]();
    }
    this.$content.html(directory.postsView.el);
    directory.postsView.render(id);
};

directory.Router = Backbone.Router.extend(rutas);

$(document).on("ready", function () {
    directory.loadTemplates(PaginasHome.getViews(),
        function () {
            directory.router = new directory.Router();
            Backbone.history.start();
        });
});

var findUrlParameter = function(sParam){
    var parameterStart = window.location.hash.indexOf("?")+1;
    if(parameterStart){
        var sPageURL = window.location.hash.substring(parameterStart);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++){
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam){
                return sParameterName[1];
            }
        }
    }

    return "";
};