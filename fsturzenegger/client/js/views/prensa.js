directory.PrensaView = Backbone.View.extend({
    render: function () {
        this.$el.html(this.template());
        loadSlider("slider-prensa","slider-prensa");
        jQuery("#navigation-images").find("a").show();
        jQuery("#prensa-navigation-box").hide();

        PostsHome.findByCategories([Categorias.PRENSA], function(col){
            Paginador.paginar(col, "#prensa?pagina=", "prensa-post-list", "prensa-paginator");
        });

        HideTwitterFeedAndShowNavigation();
        return this;
    }
});
