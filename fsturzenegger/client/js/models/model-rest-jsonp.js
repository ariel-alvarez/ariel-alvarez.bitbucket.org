var API_URL = "http://api.fedesturze.com.ar/api/";
var Categorias = {
    DESTACADO: "destacado-box",
    DESTACADO_LARGE: "destacado-large",
    BLOG: "blog",
    LEGISLATURA: "labor-parlamentaria",
    PRENSA: "prensa",
    PUBLICACIONES: "publicaciones-e-investigacion",
    PUBLICACIONES_TRABAJOS: "trabajos",
    PUBLICACIONES_TRABAJOS_PUBLICADOS: "trabajos-publicados",
    PUBLICACIONES_COLABORACIONES: "colaboraciones",
    PUBLICACIONES_LIBROS: "libros",
    NOTICIAS: "noticias"
};

var MAX_POSTS_PER_PAGE = 200;
var MAX_POSTS = 5;

directory.Post = Backbone.Model.extend({

    urlRoot: API_URL + "get_post/",

    initialize: function () {
    },

    url: function () {
        var base =
            _.result(this, 'urlRoot') || _.result(this.collection, 'url') ||
                urlError();
        if (this.isNew()) return base;
        return base.replace(/([^\/])$/, '$1') + "?id=" + encodeURIComponent(this.id);
    },
    getImageUrl:function(){
        var thumbnailUrl;
        if(this.get("thumbnail_images")){
            thumbnailUrl = this.get("thumbnail_images").full? this.get("thumbnail_images").full.url : this.get("thumbnail_images").large.url;
        }
        return thumbnailUrl;
    },
    getImageCaption:function(){
        var attachment = this.get("attachments")[0];
        if(attachment) return attachment.caption;
        return "";
    },
    generateBlogExcerpt:function(){
        var blogEntry = jQuery('<div class="blog-post row"></div>').append(
            jQuery('<div class="blog-title col-md-12"></div>')
                .append(this.getTitleAsAnchor()));

        var content = $('<div class="blog-content col-md-12"></div>');

        if(this.getImageUrl()){
            content.append($("<div class='col-md-6 excerpt-image'></div>").css("background-image","url('" + this.getImageUrl() +"')"));
            content.append($("<div class='col-md-6'></div>").append(this.getExcerptText()));
        }else{
            content.append($("<div class='col-md-12'></div>").append(this.getExcerptText()));
        }
        blogEntry.append(content);

        return blogEntry;
    },
    renderExcerptTo: function (id) {
        jQuery("#" + id).append(this.generateBlogExcerpt());
    },
    renderDatedExcerptTo: function(id){
        var excerpt = this.generateBlogExcerpt();

        var $metadata = jQuery("<div class='blog-post-fecha col-md-6'></div>").append(this.getFechaPublicacion());

        var fuente = this.getCustomField("fuente");
        if(!fuente.none){
            var $fuente = jQuery("<div class='blog-post-fecha col-md-6 text-right'></div>").append("Fuente: " + fuente);
            $metadata = jQuery("<div class='row'></div>").append($metadata).append($fuente);
        }

        excerpt.prepend($metadata);
        jQuery("#" + id).append(excerpt);
    },
    getExcerptText:function(){
        var e = this.get("excerpt");
        if(e == ''){
            e = this.get("content");
            if (e.length < 500) return e;

            var tail = e.slice(500).indexOf(" ");
            if(tail > 0) return e.slice(0,500+tail);
        }
        return e;
    },
    getFechaPublicacion: function(){
        var date = new Date(this.get("date"));
        return date.getDate() + "/"+ (date.getMonth()+1) + "/" + date.getFullYear();
    },
    getPostUrl: function(){
        return "post/" + this.get("id") + "/" + this.get("slug");
    },
    getTitleAsAnchor:function(){
        var titulo = this.get("title");
        if(titulo.length > 95) titulo = titulo.slice(0,95) + "...";
        return "<a href='#"+this.getPostUrl()+"'>"+titulo+"</a>";
    },
    getMainCategory: function(){
        var categoriaPrincipal =  _(this.get("categories")).filter(function(cat){return cat.title.toUpperCase().indexOf("DESTACADO") == -1;});
        if(categoriaPrincipal.length == 0) return Categorias.BLOG;
        return categoriaPrincipal[0].title;
    },
    getMainCategorySlug: function(){
        var categoriaPrincipal =  _(this.get("categories")).filter(function(cat){return cat.slug.toUpperCase().indexOf("destacado") == -1;});
        if(categoriaPrincipal.length == 0) return Categorias.BLOG;
        return categoriaPrincipal[0].slug;
    },
    getFacebookShareLink: function(){
        return "https://www.facebook.com/sharer/sharer.php?u=" + encodeURI(document.URL);
    },
    getTwitterShareLink: function(){
        return "https://twitter.com/home?status=" + encodeURI(document.URL);
    },
    getCustomField: function(name){
        var cf = this.get("custom_fields");
        if(cf[name] && cf[name][0]){
            return cf[name][0];
        }else{
            return {none:true};
        }
    }
});

directory.SearchPostCollection = function(query){
  return Backbone.Collection.extend({
      model: directory.Post,

      url: API_URL + "get_search_results/?search="+encodeURIComponent(query)+"&count="+MAX_POSTS_PER_PAGE+"&category_name=noticias,labor-parlamentaria,prensa,publicaciones-e-investigacion",
      parse: function (response) {
          return response.posts;
      }
  });
};

directory.PostsCollection = function (categoria, count) {
    return Backbone.Collection.extend({
        model: directory.Post,
        url: API_URL + "get_posts/?count=" + (count? count :MAX_POSTS_PER_PAGE) + (categoria? "&category_name=" + categoria : ""),
        parse: function (response) {
            return response.posts;
        },
        yearFilter:function(year){
            return _(this.filter(function(model) {
                return new Date(model.get("date")).getFullYear() === year;
            }));
//            this.reset(results);
        },
        categoryFilter:function(categoryList){
            return _(this.filter(function(model) {
                return _(model.get("categories")).filter(function(cat){return _(categoryList).any(function(c){ return cat.slug == c;})}).length > 0;
            }));
        },
        cargada : false,
        cargando: false,
        onCargada:[],
        onceLoaded: function(callback){
            var self = this;
            if(this.cargada){
                callback(this);
            } else {
                self.onCargada.push(callback);
                if(!self.cargando){
                    self.cargando = true;

                    this.fetch({success:function(){
                        self.cargada = true;
                        self.cargando = false;
                        for(var i in self.onCargada){
                            self.onCargada[i](self);
                        }
                    }});
                }
            }
        }
    });
};
directory.PostsDestacados = directory.PostsCollection(Categorias.DESTACADO, 7);
directory.PostsBlog = directory.PostsCollection(Categorias.BLOG);
directory.PostsLegislatura = directory.PostsCollection(Categorias.LEGISLATURA);
directory.PostsPrensa = directory.PostsCollection(Categorias.PRENSA);
directory.PostsPublicaciones = directory.PostsCollection(Categorias.PUBLICACIONES);

var PostsHome = {
    store : new (directory.PostsCollection())(),
    findByCategories: function(categoryList, success){
        this.store.onceLoaded(function(col){
            success(col.categoryFilter(categoryList));
        });
    },
    findByCategory: function(category, success){
        this.store.onceLoaded(function(col){
            success(col.categoryFilter([category]));
        });
    },
    all: function(success){
        this.store.onceLoaded(function(col){
            if(success) success(col);
        });
    }
};

var originalSync = Backbone.sync;
Backbone.sync = function (method, model, options) {
    if (method === "read") {
        options.dataType = "json";
        return originalSync.apply(Backbone, arguments);
    }
    return {};
};

var urlError = function () {
    throw new Error('A "url" property or function must be specified');
};

function loadSlider(containerId, category){
    var col = new (directory.PostsCollection(category, 1))();
    col.fetch({success:function(result){
        if(!result.models[0]) return;
        var images = result.models[0].attributes.content.match(/<img[\s|\S]*?\/>/g);
        var ul = jQuery('<ul class="rslides"></ul>');
        for(var i in images){
            var img = $(images[i].replace("-150x150", "")).removeAttr( "width" ).removeAttr( "height" );
            var li = $("<li></li>")
                .append(img);
            if(img.attr("alt")){
                li.append($('<p class="rslides_caption"></p>').text(img.attr("alt")))
            }
            ul.append(li);
            console.log(images[i]);
        }
        $("#"+containerId).append(ul).append('<div class="container social-slider hidden-xs">'+
            '<a href=" https://www.facebook.com/federico.sturzenegger" target="_blank"><img src="img/facebook01.png"></a>' +
            '<a href="https://twitter.com/fedesturze" target="_blank"><img src="img/twitter01.png"></a>' +
            '</div>');

        ul.responsiveSlides({
            auto: false,
            pagination: true,
            nav: true,
            pager: true,
            fade: 500
        });
    }});
}

var Paginador = {
    generarPaginas:function(col){
        var paginas = [];
        col = col.toArray();
        for (var i=0,j=col.length; i<j; i+=MAX_POSTS) {
            paginas.push(col.slice(i,i+MAX_POSTS));
        }
        return paginas;
    },
    paginar:function(col, hashSelector, renderListTo, paginadorContainer){
        var siguiente = 0;
        var anterior = 0;
        var numeroDePagina = this.pageNumber();
        var pages = this.generarPaginas(col);

        if(numeroDePagina > pages.length){
            numeroDePagina = 0;
        }

        if(numeroDePagina < pages.length-1){
            siguiente = numeroDePagina +1;
        } else {
            siguiente = numeroDePagina;
        }

        if(numeroDePagina){
            anterior = numeroDePagina - 1;
        }
        this.renderPages(pages[numeroDePagina], renderListTo);

        var paginator = $("#"+paginadorContainer);
        paginator.html("");
        paginator.append($('<li><a href="'+hashSelector+anterior+'">&laquo;</a></li>'));
        _(pages).each(function(p, i){
            paginator.append($('<li><a href="'+hashSelector+i+'">'+ (i+1) +'</a></li>').addClass(numeroDePagina == i? "pagina-seleccionada":""));
        });
        paginator.append($('<li><a href="'+hashSelector+siguiente+'">&raquo;</a></li>'));
    },
    pageNumber:function(){
      return Number(findUrlParameter("pagina"));
    },
    renderPages:function(pages,renderListTo){
        _(pages).each(function(aPost){
            aPost.renderDatedExcerptTo(renderListTo);
        });
    }
};