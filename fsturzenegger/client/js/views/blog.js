directory.BlogView = Backbone.View.extend({

    render: function () {
        this.$el.html(this.template());
        jQuery("#navigation-images").find("a").show();
        jQuery("#blog-navigation-box").hide();
        loadSlider("slider-blog","slider-blog");

        PostsHome.findByCategories([Categorias.BLOG], function(col){
            Paginador.paginar(col, "#blog?pagina=", "blog-post-list", "blog-paginator");
        });

        HideTwitterFeedAndShowNavigation();
        return this;
    }
});

