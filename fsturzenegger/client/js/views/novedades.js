directory.NovedadesView = Backbone.View.extend({

    events:{
        "click #novedades-search-button": "onSearchClick",
        "keyup #novedades-search-button": "onSearchClick"
    },
    initialize: function(){
        return this;
    },
    renderList : function(col){
        this.$el.html(this.template());
        col.each(function(e){
            e.renderDatedExcerptTo("novedades-post-list");
        });
        return this;
    },
    renderSearchList: function(query){
        var self = this;
        var col = new (directory.SearchPostCollection(query))();
        col.fetch({success:function(result){
            self.renderList(result);
        }});
    },
    render: function () {

        this.$el.html(this.template());
        PostsHome.findByCategories([Categorias.BLOG, Categorias.PRENSA, Categorias.LEGISLATURA], function(col){
            col.each(function(aPost){
                aPost.renderDatedExcerptTo("novedades-post-list");
            });
            jQuery("#novedades-post-list").append(TWITTER_SHARE_BUTTON);
        });
        HideTwitterFeedAndShowNavigation();
        return this;
    },
    search: function(year){
        var self = this;
        PostsHome.all(function(col){
            self.renderList(col.yearFilter(year));
        });
    },
    onSearchClick: function(){
        var year = Number(jQuery("#novedades-search-input").val());
        if(year){
            this.search(year);
        }
    }
});

var TWITTER_SHARE_BUTTON = "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
