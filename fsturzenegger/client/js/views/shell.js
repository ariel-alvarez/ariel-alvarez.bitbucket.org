directory.ShellView = Backbone.View.extend({

    initialize: function () {
    },

    render: function () {
        this.$el.html(this.template());
        return this;
    },

    events: {
        "keyup .search-query": "onkeypress"
    },

    search: function (event) {
        var key = $('.search-query').val();
        if (!directory.busquedaView) {
            directory.busquedaView = new directory.BusquedaView();
        }
        directory.busquedaView.renderSearchList(key);
        $("#content").html(directory.busquedaView.el);
        document.location.hash = "busqueda"
    },

    onkeypress: function (event) {
        if (event.keyCode === 13) { // enter key pressed
            event.preventDefault();
            this.search(event);
        }
    },

    selectMenuItem: function(menuItem) {
        $('.navbar li').removeClass('active');
        if (menuItem) {
            $('.' + menuItem).addClass('active');
        }
    }

});

function RenderTwitter(){
    jQuery("#navigation-images").hide();
    jQuery("#imagen-social-twitter").hide();
    jQuery("#twitter-feed").show();
}

function HideTwitterFeedAndShowNavigation(){
    jQuery("#twitter-feed").hide();
    jQuery("#navigation-images").show();
    jQuery("#imagen-social-twitter").show();
}